<?php
require('functions.php');
$get = isset( $_GET['karyawan'] ) ? $_GET['karyawan'] : '';
?>

<!DOCTYPE html>
<html>
<head>
	<title>CRUD - PHP MySQL</title>
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<h2 class="header">CRUD - PHP MySQL</h2>

		<?php if ( $get == '' ): ?>

			<?php include('show.php') ?>

		<?php elseif( $get == 'create' ): ?>

			<?php include('create.php') ?>

		<?php elseif( $get == 'edit' ): ?>

			<?php include('edit.php') ?>

		<?php elseif( $get == 'delete' ): ?>

			<?php
			$id = $_GET['id'];
			$Karyawan->delete($id);
			?>

		<?php endif ?>
	
</body>
</html>