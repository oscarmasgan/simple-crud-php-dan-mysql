<?php
require('functions.php');
$search = $_GET['search'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>CRUD - PHP MySQL</title>
	<link rel="stylesheet" href="style.css">
</head>

<body>

	<link rel="stylesheet" type="text/css" href="style.css">

	<h2 class="header">CRUD - PHP MySQL</h2>

	<table class="table">
		<tr>
			<td colspan="2" class="create-data">
				<a href="index.php?karyawan=create">Tambah Data</a>
			</td>
			<td colspan="4" class="create-data">
				<a href="index.php"><< Kembali</a>
			</td>
			<td colspan="1" class="search-data">
				<form method="GET" action="search.php">
					<input type="text" name="search" placeholder="Search by Nama...">
				</form>
			</td>
		</tr>
		<tr>
			<th>Nama</th>
			<th>Gaji</th>
			<th>Tanggal Lahir</th>
			<th>Jenis Kelamin</th>
			<th>Hoby</th>
			<th>Pesan</th>
			<th>Action</th>
		</tr>
		<?php foreach ($Karyawan->search( $search ) as $value): ?>
			<tr>
				<td><?= $value['nama'] ?></td>
				<td><?= $Karyawan->gaji( $value['gaji'] ) ?></td>
				<td><?= $value['tanggal_lahir'] ?></td>
				<td><?= $value['gender'] ?></td>
				<td>
					<?php
					if ( $value['hobi'] !== 'null' ) {
						$hobies = json_decode( $value['hobi'] );
						foreach ($hobies as $key2 => $value2) {
							echo '<p>> '.$value2.'</p>';
						}
					}else{
						echo 'Orang ini tidak punya hobi';	
					}
					?>	
				</td>
				<td>
					<?= $value['pesan'] ?>
				</td>
				<td style="width: 100px; text-align: center;">
					<a href="index.php?karyawan=edit&id=<?= $value['id'] ?>">Edit</a>
					||
					<a href="index.php?karyawan=delete&id=<?= $value['id'] ?>" onclick="return confirm('Anda ingin menghapusnya ?')">Delete</a>
				</td>
			</tr>
		<?php endforeach ?>
	</table>

</body>
</html>