<?php
$id = $_GET['id'];
$data = $Karyawan->edit($id);
$hobies = json_decode($data->hobi);
?>

<p style="text-align: center"><b>Edit Data Karyawan</b></p>
<form method="POST" action="functions.php">
	<input type="hidden" name="id" value="<?= $id ?>">
	<table>
		<tr><td style="border: none;"><a href="index.php">Kembali</a></td></tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td>
				<input type="text" name="nama" value="<?= $data->nama ?>">
			</td>
		</tr>
		<tr>
			<td>Gaji</td>
			<td>:</td>
			<td>
				<input type="text" name="gaji" value="<?= $data->gaji ?>">
			</td>
		</tr>
		<tr>
			<td>Tanggal Lahir</td>
			<td>:</td>
			<td>
				<input type="date" name="tanggal_lahir" value="<?= $data->tanggal_lahir ?>">
			</td>
		</tr>
		<tr >
			<td>Jenis Kelamin</td>
			<td>:</td>
			<td>
				<input type="radio" name="gender" value="pria" id="pria" <?= $data->gender == 'pria' ? 'checked' : '' ?> > <label for="pria">Pria</label>
				||
				<input type="radio" name="gender" value="wanita" id="wanita" <?= $data->gender == 'wanita' ? 'checked' : '' ?>> <label for="wanita">Wanita</label>
			</td>
		</tr>
		<tr>
			<td>Hobi</td>
			<td>:</td>
			<td>
				<?php foreach ($Karyawan->data_hobies as $key => $value): ?>
					<input type="checkbox" name="hobi[]" value="<?= $value ?>" id="<?= $key ?>" 
					<?php 
					if ( $hobies !== null && in_array( $value, $hobies) ) {
						 echo 'checked';
					}	
					?> 
					> 
					<label for="<?= $key ?>"><?= $value ?></label> <br>
				<?php endforeach ?>
			</td>
		</tr>
		<tr>
			<td>Pesan</td>
			<td>:</td>
			<td>
				<textarea name="pesan" rows="5" cols="25"><?= $data->pesan ?></textarea>
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>
				<input type="submit" name="update" value="Rubah Data">
			</td>
		</tr>
	</table>
</form>