<?php
class Karyawan{

	private $host = 'localhost';
	private $user = 'root';
	private $pass = '';
	private $dbname = 'crud';
    private $pages = '';

    public function __construct(){
    	$this->conn = new PDO('mysql:host='.$this->host.'; dbname='.$this->dbname.'', $this->user, $this->pass);
        $this->data_hobies = array( 'Sepak Bola', 'Basket', 'Catur', 'Berenang' );
    }

    public function show(){
    	$sql = "SELECT*FROM karyawan";
    	$stmt = $this->conn->prepare($sql);
    	$stmt->execute();
    	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
    	return $rows;
    }

    public function select(){
        $halaman = 5;

        if ( isset( $_GET['halaman'] ) ) {
            $page = (int)$_GET['halaman'];
        }else{
            $page = 1;
        }

        $start = ($page > 1) ? ( $page * $halaman ) - $halaman : 0;

        $total_rows = $this->show();
        $total = count($total_rows);
        $this->pages = ceil($total/$halaman);

        $sql = "SELECT*FROM karyawan LIMIT $start, $halaman";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function pagination(){
        for ($i = 1; $i <= $this->pages ; $i++) { ?>
            <a href="?halaman=<?= $i ?>"><?= $i ?></a>
        <?php }
    }

    public function create( $nama, $gaji, $tanggal_lahir, $gender, $hobi, $pesan ){
    	$id = '';
    	$sql = 'INSERT INTO karyawan VALUES (:id, :nama, :gaji, :tanggal_lahir, :gender, :hobi, :pesan)';
    	$stmt = $this->conn->prepare($sql);
    	$stmt->bindParam(':id', $id);
    	$stmt->bindParam(':nama', $nama);
    	$stmt->bindParam(':gaji', $gaji);
    	$stmt->bindParam(':tanggal_lahir', $tanggal_lahir);
    	$stmt->bindParam(':gender', $gender);
    	$stmt->bindParam(':hobi', $hobi);
    	$stmt->bindParam(':pesan', $pesan);
    	$stmt->execute();
    	header('location: index.php');
    }

    public function edit( $id ){
    	$sql = 'SELECT*FROM karyawan WHERE id = :id';
    	$stmt = $this->conn->prepare($sql);
    	$stmt->bindParam(':id', $id);
    	$stmt->execute();
    	$row = $stmt->fetch(PDO::FETCH_OBJ);
    	return $row;
    }

    public function update( $id, $nama, $gaji, $tanggal_lahir, $gender, $hobi, $pesan ){
    	$sql = 'UPDATE karyawan 
                SET nama = :nama, 
                    gaji = :gaji, 
                    tanggal_lahir = :tanggal_lahir, 
                    gender = :gender, 
                    hobi = :hobi, 
                    pesan = :pesan WHERE id = :id';
    	$stmt = $this->conn->prepare($sql);
    	$stmt->bindParam(':id', $id);
    	$stmt->bindParam(':nama', $nama);
        $stmt->bindParam(':gaji', $gaji);
        $stmt->bindParam(':tanggal_lahir', $tanggal_lahir);
        $stmt->bindParam(':gender', $gender);
        $stmt->bindParam(':hobi', $hobi);
        $stmt->bindParam(':pesan', $pesan);
    	$stmt->execute();

    	header('location: index.php');
    }

    public function delete( $id ){
    	$sql = 'DELETE FROM karyawan WHERE id = :id';
    	$stmt = $this->conn->prepare($sql);
    	$stmt->bindParam(':id', $id);
    	$stmt->execute();
    	header('location: index.php');
    }

    public function search( $search ){
        $sql = "SELECT*FROM karyawan WHERE nama LIKE '%".$search."%'";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $rows;
    }

    public function gaji( $fee ){
        $gaji = "Rp ".number_format( $fee, 2, ',', '.' );
        return $gaji;
    }

}

$Karyawan = new Karyawan;

if ( isset($_POST['create']) ) {
	$nama = $_POST['nama'];
	$gaji = $_POST['gaji'];
	$tanggal_lahir = $_POST['tanggal_lahir'];
	$gender = $_POST['gender'];
	$hobi = json_encode( $_POST['hobi'] );
	$pesan = $_POST['pesan'];
	$Karyawan->create( $nama, $gaji, $tanggal_lahir, $gender, $hobi, $pesan );
}elseif( isset($_POST['update']) ){
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $gaji = $_POST['gaji'];
    $tanggal_lahir = $_POST['tanggal_lahir'];
    $gender = $_POST['gender'];
    $hobi = json_encode( $_POST['hobi'] );
    $pesan = $_POST['pesan'];
    $Karyawan->update( $id, $nama, $gaji, $tanggal_lahir, $gender, $hobi, $pesan );
}