<p style="text-align: center"><b>Tambah Data Karyawan</b></p>
<form method="POST" action="functions.php">
	<table>
		<tr><td style="border: none;"><a href="index.php">Kembali</a></td></tr>
		<tr>
			<td>Nama</td>
			<td>:</td>
			<td>
				<input type="text" name="nama">
			</td>
		</tr>
		<tr>
			<td>Gaji</td>
			<td>:</td>
			<td>
				<input type="text" name="gaji">
			</td>
		</tr>
		<tr>
			<td>Tanggal Lahir</td>
			<td>:</td>
			<td>
				<input type="date" name="tanggal_lahir">
			</td>
		</tr>
		<tr >
			<td>Jenis Kelamin</td>
			<td>:</td>
			<td>
				<input type="radio" name="gender" value="pria" id="pria"> <label for="pria">Pria</label>
				||
				<input type="radio" name="gender" value="wanita" id="wanita"> <label for="wanita">Wanita</label>
			</td>
		</tr>
		<tr>
			<td>Hobi</td>
			<td>:</td>
			<td>
				<?php foreach ($Karyawan->data_hobies as $key => $value): ?>
					<input type="checkbox" name="hobi[]" value="<?= $value ?>" id="<?= $key ?>"> <label for="<?= $key ?>"><?= $value ?></label> <br>
				<?php endforeach ?>
			</td>
		</tr>
		<tr>
			<td>Pesan</td>
			<td>:</td>
			<td>
				<textarea name="pesan" rows="5"></textarea>
			</td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td>
				<input type="submit" name="create" value="Tambah Data">
			</td>
		</tr>
	</table>
</form>